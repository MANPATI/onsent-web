//************** */ for index.html * *******************

var keyval = {
  nameTab: "Onsent",

  // *********** HEADER ***********

  //===== navbar

  // ********** PRODUCT PACKAGE **********
  productTitle: "PRODUCT ONSENT",
  productTitle2: "ONSENT",
  desctipProduct:
    "Lorem ipsum dolor sit atmet sit dolor greand fdanrh sdfs sit atmet sit dolor greand fdanrh sdfs",
  per01: "per month",
  per02: "per month",
  per03: "per month",
  spann: "$",

  freePack: "FREE",
  goldPack: "GOLD",
  platinumPack: "PLATINUM",

  priceFree: "0",
  description01: "Lorem ipsum dolor sit atmet sit dolor greand fdanrh s",
  desc11: "5 Active Event",
  desc12: "30 Day Trial",
  desc13: "-",
  desc14: "-",
  buttonpack01: "Get started",

  priceGold: "15",
  description02: "Lorem ipsum dolor sit atmet sit dolor greand fdanrh s",
  desc21: "5 Active Event",
  desc22: "30 Day Trial",
  desc23: "No Booking Fees",
  desc24: "-",
  buttonpack02: "Get started",

  pricePlatinum: "29",
  description03: "Lorem ipsum dolor sit atmet sit dolor greand fdanrh s",
  desc31: "5 Active Event",
  desc32: "30 Day Trial",
  desc33: "No Booking Fees",
  desc34: "30 Day Trial",
  buttonpack03: "Get started",
};

$.each(keyval, function (key, val) {
  $("#" + key).text(val);
});

var keyth = {
  // ********** PRODUCT PACKAGE **********
  productTitle: "ผลิตภัณฑ์ของออนเซ็น",
  productTitle2: "ONSENT",
  desctipProduct:
    "Lorem ipsum dolor sit atmet sit dolor greand fdanrh sdfs sit atmet sit dolor greand fdanrh sdfs",
  per01: "รายเดือน",
  per02: "รายเดือน",
  per03: "รายเดือน",
  spann: "$",

  freePack: "FREE",
  goldPack: "GOLD",
  platinumPack: "PLATINUM",

  priceFree: "0",
  description01: "Lorem ipsum dolor sit atmet sit dolor greand fdanrh s",
  desc11: "5 Active Event",
  desc12: "30 Day Trial",
  desc13: "-",
  desc14: "-",
  buttonpack01: "Get started",

  priceGold: "15",
  description02: "Lorem ipsum dolor sit atmet sit dolor greand fdanrh s",
  desc21: "5 Active Event",
  desc22: "30 Day Trial",
  desc23: "No Booking Fees",
  desc24: "-",
  buttonpack02: "Get started",

  pricePlatinum: "29",
  description03: "Lorem ipsum dolor sit atmet sit dolor greand fdanrh s",
  desc31: "5 Active Event",
  desc32: "30 Day Trial",
  desc33: "No Booking Fees",
  desc34: "30 Day Trial",
  buttonpack03: "Get started",
};

$.each(keyth, function (key, val) {
  $("#" + key).text(val);
});

let items = [];
let test;

$.getJSON(`assets/text.json`, function (data) {
  //var items = [];
  // $.each( data, function( key, val ) {
  //   items.push(val);
  // });
  // console.log("hhh", data);
  test = data;
  set_lang(test.english);
});
///translate

// Some variables for later
var dictionary, set_lang;

// Object literal behaving as multi-dictionary
dictionary = {
  english: {
    _hello: "Hello",
    _january: "January",
  },
  portuguese: {
    _hello: "Oie",
    _january: "Janeiro",
  },
  russian: {
    _hello: "привет",
    _january: "январь",
  },
};

set_lang = function (dictionary) {
  $("[data-translate]").text(function () {
    var key = $(this).data("translate");
    if (dictionary.hasOwnProperty(key)) {
      return dictionary[key];
    }
  });
};

// Swap languages when menu changes
$("#lang").on("change", function () {
  var language = $(this).val().toLowerCase();
  if (test.hasOwnProperty(language)) {
    set_lang(test[language]);
  }
});

// const subscribeApi = "http://54.169.88.28:4100/v1/email/subscribe"
const subscribeApi = "http://localhost:4100/v1/email/subscribe";
$("#sendmail").click(function (event) {
  const email = document.getElementById("email");
  const company = document.getElementById("company");
  if (!company.value || !email.value) {
    alert("please input Your Companey and Email !!");
    return;
  }
  $.ajax({
    method: "POST",
    dataType: "json",
    url: subscribeApi,
    data: {
      email: email.value,
      company: company.value,
    },
    success: function (response) {
      if (response.result) {
        alert(response.message);
        email.value = "";
        company.value = "";
      }
    },
    error: function (res, status, error) {
      alert("please input Your Companey and Email !!");
    },
  });
});

// FIRST INPUT
$("#getstarted").click(function (event) {
  const email = document.getElementById("inputmail");
  if (!email.value) {
    alert("please input Your Email !!");
    return;
  }
  $.ajax({
    type: "POST",
    dataType: "json",
    url: subscribeApi,
    data: {
      email: `${email.value}`,
    },
    success: function (response) {
      if (response.result) {
        alert(response.message);
        email.value = "";
      }
    },
    error: function (res, status, error) {
      alert("please input Your Email !!");
    },
  });
});

$("#right-button").click(function () {
  event.preventDefault();
  $("#content").animate(
    {
      scrollLeft: "+=1000px",
    },
    "slow"
  );
});

$("#left-button").click(function () {
  event.preventDefault();
  $("#content").animate(
    {
      scrollLeft: "-=1000px",
    },
    "slow"
  );
});

// Set initial language to English
